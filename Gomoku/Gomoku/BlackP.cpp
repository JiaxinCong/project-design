//
//  BlackP.cpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#include "BlackP.hpp"
BlackP::BlackP(){
    blackCircle.setRadius(15.0); //set the Radius of black piece
    blackCircle.setOrigin(15.0, 15.0); // set the Original position for the piece
    blackCircle.setFillColor(sf::Color::Black);// set the color of the piece black
}

BlackP::~BlackP(){
    
}//Destructer

sf::Vector2f BlackP::getPosition(){
    return blackCircle.getPosition();
}//get the position of the black piece

void BlackP::draw(sf::RenderWindow &window){
    window.draw(blackCircle);
}// draw the black piece in the window


void BlackP::setPosition(float x, float y){
    blackCircle.setPosition(x, y);
}// set the position of the blacke piece in the board
