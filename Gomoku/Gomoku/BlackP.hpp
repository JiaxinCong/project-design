//
//  BlackP.hpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#ifndef BlackP_hpp
#define BlackP_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
class BlackP{
public:
    BlackP();
    ~BlackP();
    
    void draw(sf::RenderWindow &window);
    void setPosition(float x, float y);
    sf::Vector2f getPosition();
private:
    sf::CircleShape blackCircle;
};
#endif /* BlackP_hpp */
