//
//  GameLogic.hpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#ifndef GameLogic_hpp
#define GameLogic_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
class GameLogic{
public:
    GameLogic();
    bool check(sf::Vector2f point, int color);
    int checkValid(sf::Vector2f point);
    void loadPosition(sf::Vector2f point, int player);
    int getsize();
    void printBoard();
private:
    int board[15][15];
    int size;
};
#endif /* GameLogic_hpp */
