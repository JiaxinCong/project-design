//
//  WhiteP.cpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#include "WhiteP.hpp"
WhiteP::WhiteP(){
    whiteCircle.setRadius(15.0);// set the Radius of black piece
    whiteCircle.setOrigin(15.0, 15.0); // set the Orginal position for the piece
    whiteCircle.setFillColor(sf::Color::White); // set the color of the piece black
}

WhiteP::~WhiteP(){
    
}// Destructer

void WhiteP::draw(sf::RenderWindow &window){
    window.draw(whiteCircle);
} // get the position of the white piece


void WhiteP::setPosition(float x, float y){
    whiteCircle.setPosition(x, y);
} // draw the black piece in the window

sf::Vector2f WhiteP::getPosition(){
    return whiteCircle.getPosition();
} // set the position of the black piece in the board
