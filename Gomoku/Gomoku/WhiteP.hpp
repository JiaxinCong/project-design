//
//  WhiteP.hpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#ifndef WhiteP_hpp
#define WhiteP_hpp
#include <SFML/Graphics.hpp>
#include <stdio.h>
class WhiteP{
public:
    WhiteP();
    ~WhiteP();
    
    void draw(sf::RenderWindow &window);
    void setPosition(float x, float y);
    sf::Vector2f getPosition();
private:
    sf::CircleShape whiteCircle;
};
#endif /* WhiteP_hpp */
