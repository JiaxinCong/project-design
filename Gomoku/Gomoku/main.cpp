

#include <SFML/Graphics.hpp>
#include "WhiteP.hpp"
#include "BlackP.hpp"
#include "GameLogic.hpp"
#include "ResourcePath.hpp"
#include <iostream>
using namespace sf;

int main(int, char const**){
    GameLogic game;
    RenderWindow window(VideoMode(600, 600), "SFML Gomoku");//windows name SFML Gomoku
    
    int size = game.getsize();
    // Set the Icon
    Texture boardBack; //create a texture as board
    if (!boardBack.loadFromFile(resourcePath()+ "chessboard.png")) { // import board image
        return EXIT_FAILURE;
    }
    
    Texture blackwin;
    blackwin.loadFromFile(resourcePath() + "BlackWin.png"); // import BlackWin image
    Sprite blacks(blackwin);
    Texture whitewin;
    whitewin.loadFromFile( resourcePath()+ "WhiteWin.png");// import WhiteWin image
    Sprite whites(whitewin);
    Sprite sprite(boardBack);
    
    BlackP black;
    WhiteP white;
    
    std::vector<WhiteP> circleWhiteList; //black chess
    std::vector<BlackP> circleBlackList;//white chess
    
    int turn = 0;//set turn for each person
    int win = 0; //virable to decided who win after cilcker the mouse.
    
    while(window.isOpen()){
        Event event;
        while (window.pollEvent(event)){
            // Close window: exit
            if (event.type == Event::Closed) {
                window.close();
            }
            
            // Escape pressed: exit
            if (event.type == Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
            
            if(turn == 0){
                if(Mouse::isButtonPressed(Mouse::Left)){
                    Vector2i pos = Mouse::getPosition(window);
                    black.setPosition((float)pos.x,(float)pos.y);
                    //                std::cout << "Left mouse button pressed" << std::endl;
                }// read mouse click position
                if(event.type == Event::MouseButtonReleased){
                    if(event.mouseButton.button == Mouse::Left){
                        Vector2f p = black.getPosition();
                        Vector2f newPos;
                        if(((p.x-20.0f)/size - int((p.x-20.0f)/size)) >= 0.5){
                            newPos.x = size*int((p.x-20.0f)/size+1) +20;
                        }
                        else{
                            newPos.x = size*int((p.x-20.0f)/size) +20;
                        }
                        if(((p.y-20.0f)/size - int((p.y-20.0f)/size)) >= 0.5){
                            newPos.y = size*int((p.y-20.0f)/size+1) +20;
                        }
                        else{
                            newPos.y = size*int((p.y-20.0f)/size) +20;
                        }//meshing the board and decided which part is most close to the click position
                        //Vector2f newPos = Vector2f(size*int(((p.x-20.0f)/size))+20, size*int(((p.y-20.0f)/size))+20);
                        if(game.checkValid(newPos)){ // check if there is chess before
                            turn = 1;
                            black.setPosition(newPos.x, newPos.y);
                            game.loadPosition(newPos, 1); //load position to virture board
                            std::cout<<std::endl;
                            game.printBoard();
                            circleBlackList.push_back(black); //load position which need to draw
                            if(game.check(newPos, 1)){
                                std::cout<<"WININ"<<std::endl;
                                win = 1;//check who is win
                            }
                        }
                    }
                }
            }
            
            else if(turn == 1){//same idea as pre
                if(Mouse::isButtonPressed(Mouse::Left)){
                    Vector2i pos = Mouse::getPosition(window);
                    white.setPosition((float)pos.x,(float)pos.y);
                }
                
                if(event.type == Event::MouseButtonReleased){
                    if(event.mouseButton.button == Mouse::Left){
                        Vector2f p = white.getPosition();
                        Vector2f newPos;
                        if(((p.x-20.0f)/size - int((p.x-20.0f)/size)) >= 0.5){
                            newPos.x = size*int((p.x-20.0f)/size+1) +20;
                        }
                        else{
                            newPos.x = size*int((p.x-20.0f)/size) +20;
                        }
                        if(((p.y-20.0f)/size - int((p.y-20.0f)/size)) >= 0.5){
                            newPos.y = size*int((p.y-20.0f)/size+1) +20;
                        }
                        else{
                            newPos.y = size*int((p.y-20.0f)/size) +20;
                        }
                        
                        if(game.checkValid(newPos)){
                            
                            turn = 0;
                            white.setPosition(newPos.x, newPos.y);
                            game.loadPosition(newPos, 2);
                            std::cout<<std::endl;
                            game.printBoard();
                            circleWhiteList.push_back(white);
                            if(game.check(newPos, 2)){
                                win = 2;
                            }
                        }
                    }
                }
            }
        }
        window.clear();
        
        // Draw the sprite
        window.draw(sprite);
        for(std::vector<BlackP>::iterator iterB = circleBlackList.begin(); iterB!=circleBlackList.end(); iterB++){
            iterB->draw(window);
        }
        //draw piece
        for(std::vector<WhiteP>::iterator iterW = circleWhiteList.begin(); iterW!=circleWhiteList.end(); iterW++){
            iterW->draw(window);
        }
        
        if(win == 1){// draw image when win
            window.clear();
            window.draw(blacks);
        }
        else if(win == 2){
            window.clear();
            window.draw(whites);
        }
        
        // Update the window
        window.display();
    }
}
