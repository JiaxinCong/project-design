//
//  GameLogic.cpp
//  Gomoku
//
//  Created by 丛嘉鑫 on 3/14/19.
//  Copyright © 2019 丛嘉鑫. All rights reserved.
//

#include "GameLogic.hpp"
#include <iostream>
GameLogic::GameLogic(){
    size = 40;
    for(int i = 0; i<15; i++){
        for(int j = 0; j<15; j++){
            board[i][j] = 0;
        }
    }
} // initial the board

int GameLogic::getsize(){
    return size;//set small rect size
}

int GameLogic::checkValid(sf::Vector2f point){//check the position is valid
    if((board[int((point.y-20.0f)/size)][int((point.x-20.0f)/size)]) != 0){
        return 0;
    }
    else{
        return 1;
    }
}

void GameLogic::loadPosition(sf::Vector2f point, int player){//load piece position to virtual board
    if (checkValid(point)){
        board[int((point.y-20.0f)/size)][int((point.x-20.0f)/size)] = player;
    }
}

bool GameLogic::check(sf::Vector2f point, int color){
    //check the column
    int col = int((point.x-20.0f)/size);
    int row = int((point.y-20.0f)/size);
    int iffive[12]; //memory Boolean list
    
    for(int i = 0; i< 12; i++){
        iffive[i] = 1;//initial the list
    }
    if(board[row][col-1] == color && board[row][col+1] == color){//check the row with 0x0
        iffive[0] = 1;
        for(int i = 1; i < 5; i++){
            if((col - i) < 0){
                break;
            }
            else if(board[row][col - i] == color){
                iffive[0]++;
            }
            else if(board[row][col - i] != color){
                break;
            }
        }
        
        for(int i = 1; i < 5; i++){
            if((col + i) > 14){
                break;
            }
            else if(board[row][col + i] == color){
                iffive[0]++;
            }
            else if(board[row][col + i] != color){
                break;
            }
        }
    }
        
    
    if(board[row][col-1] == color && board[row][col+1] != color){//check 0x!
        iffive[1] = 1;
        for(int i = 1; i < 5; i++){
            if((col - i) < 0){
                break;
            }
            else if(board[row][col - i] == color){
                iffive[1]++;
            }
            else if(board[row][col - i] != color){
                break;
            }
        }
    }
    
    if(board[row][col-1] != color && board[row][col+1] == color){//check !x0
        iffive[2] = 1;
        for(int i = 1; i < 5; i++){
            if((col + i) > 14){
                break;
            }
            else if(board[row][col + i] == color){
                iffive[2]++;
            }
            else if(board[row][col + i] != color){
                break;
            }
        }
    }
    
    //check the col
    if(board[row+1][col] == color && board[row-1][col] == color){//check 0
                                                                    //x
                                                                    //0
        iffive[3] = 1;
        for(int i = 1; i < 5; i++){
            if((row - i) < 0){
                break;
            }
            else if(board[row-i][col] == color){
                iffive[3]++;
            }
            else if(board[row-i][col] != color){
                break;
            }
        }
        
        for(int i = 1; i < 5; i++){
            if((row+i)>14){
                break;
            }
            else if(board[row+i][col] == color){
                iffive[3]++;
            }
            else if(board[row+i][col] != color){
                break;
            }
        }
    }
    
    if(board[row-1][col] == color && board[row+1][col] != color){//same idea as row
        iffive[4] = 1;
        for(int i = 1; i < 5; i++){
            if((row - i) < 0){
                break;
            }
            else if(board[row-i][col] == color){
                iffive[4]++;
            }
            else if(board[row-i][col] != color){
                break;
            }
        }
    }
    
    if(board[row-1][col] != color && board[row+1][col] == color){
        iffive[5] = 1;
        for(int i = 1; i < 5; i++){
            if(board[row+i][col] == color){
                iffive[5]++;
            }
            else if(board[row+i][col] != color){
                break;
            }
        }
    }
    
    //check the up and down left
    if(board[row-1][col-1] == color && board[row+1][col+1] == color){
        iffive[6] = 1;
        for(int i = 1; i < 5; i++){
            if(((row - i) < 0)||((col - i) < 0)){
                break;
            }
            else if(board[row-i][col-i] == color){
                iffive[6]++;
            }
            else if(board[row-i][col - i] != color){
                break;
            }
        }
        
        for(int i = 1; i < 5; i++){
            if(((row + i) > 14)||((col + i) >14)){
                break;
            }
            if(board[row+i][col + i] == color){
                iffive[6]++;
            }
            else if(board[row+i][col + i] != color){
                break;
            }
        }
        
    }
    
    if(board[row-1][col-1] == color && board[row+1][col+1] != color){
        iffive[7] = 1;
        for(int i = 1; i < 5; i++){
            if(((row - i) < 0)||((col -i) < 0)){
                break;
            }
            else if(board[row-i][col - i] == color){
                iffive[7]++;
            }
            else if(board[row-i][col - i] != color){
                break;
            }
        }
    }
    
    if(board[row-1][col-1] != color && board[row+1][col+1] == color){
        iffive[8] = 1;
        for(int i = 1; i < 5; i++){
            if((row+i) > 14 || (col+i)>14){
                break;
            }
            else if(board[row+i][col+i] == color){
                iffive[8]++;
            }
            else if(board[row+i][col+i] != color){
                break;
            }
        }
    }
    
    //check the left up and down
    if(board[row-1][col+1] == color && board[row+1][col-1] == color){
        for(int i = 1; i < 5; i++){
            if(((row - i) < 0)||((col + i) > 14)){
                break;
            }
            else if(board[row-i][col+i] == color){
                iffive[9]++;
            }
            else if(board[row-i][col+i] != color){
                break;
            }
        }
        
        for(int i = 1; i < 5; i++){
            if(((col - i) < 0)||((row + i) > 14)){
                break;
            }
            else if(board[row+i][col-i] == color){
                iffive[9]++;
            }
            else if(board[row+i][col-i] != color){
                break;
            }
        }
        
    }
    
    if(board[row-1][col+1] == color && board[row+1][col-1] != color){
        for(int i = 1; i < 5; i++){
            if(((row - i) < 0)||((col + i) > 14 )){
                break;
            }
            else if(board[row-i][col+i] == color){
                iffive[10]++;
            }
            else if(board[row-i][col+i] != color){
                break;
            }
        }
    }
    
    if(board[row-1][col+1] != color && board[row+1][col-1] == color){
        for(int i = 1; i < 5; i++){
            if(((row + i) > 14)||((col -i) < 0)){
                break;
            }
            else if(board[row+i][col-i] == color){
                iffive[11]++;
            }
            else if(board[row+i][col-i] != color){
                break;
            }
        }
    }
    
    for(int i = 0; i<12 ; i++){
        if(iffive[i] >= 5){
            return true;//check if any logic is true return win
        }
    }
    
    return false;
}

void GameLogic::printBoard(){//print board
    for(int i = 0; i< 15; i++){
        for(int j = 0; j< 15; j++){
            std::cout<<board[i][j]<<" ";
        }
        std::cout<<std::endl;
    }
}


